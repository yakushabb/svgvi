<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
  <!-- Copyright 2019 PWMC Services S.A.S. de C.V. -->
  <!-- Copyright 2022 Daniel Espinosa -->
  <id>mx.pwmc.Svgvi</id>
  <launchable type="desktop-id">mx.pwmc.Svgvi.desktop</launchable>
  <provides>
    <id>mx.pwmc.Svgvi.desktop</id>
  </provides>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>
  <name>Svgvi</name>
  <summary>SVG viewer and editor using XML declarations</summary>
  <update_contact>esodan@gmail.com</update_contact>
  <url type="homepage">https://gitlab.com/gsvg/svgvi/-/wikis/home</url>
  <url type="bugtracker">https://gitlab.com/gsvg/svgvi/-/issues</url>
  <translation>https://gitlab.com/gsvg/svgvi/</translation>
  <description>
    <p>SVG file viewer and editor, with preview on changes made on XML definition</p>
  </description>
  <screenshots>
    <screenshot type="default">
      <caption>Svgvi Logo</caption>
      <image type="source" width="1471" height="900">https://gitlab.com/gsvg/svgvi/-/wikis/uploads/4cb8d4931d56e9069bd9776abd4a7611/svgvi2.png</image>
    </screenshot>
  </screenshots>
  <content_rating type="oars-1.1" />
  <releases>
    <release type="stable" date="2022-09-08" urgency="low" version="2.0.2">
      <size type="download">7858728</size>
      <description>
        <p>Ported to GTK 4.0</p>
        <p>Updated dependencies</p>
      </description>
    </release>
    <release version="1.6.0" date="2019-08-06" urgency="low" type="stable">
      <size type="download">9876592</size>
      <description>
        <p>Introduced new toolbox to insert basic shapes and text definitions</p>
        <p>Now any text introduced is saved even if it is not a valid SVG to increase fault tolerant</p>
        <p>Updated dependencies</p>
      </description>
    </release>
    <release version="1.4.0" date="2019-03-26" urgency="low" type="stable">
      <size type="download">9876592</size>
      <description>
        <p>Better search capabilities</p>
        <p>Better no UI block while loading or rendering files</p>
        <p>Added filters for open/save as dialogs</p>
        <p>Updated dependencies</p>
        <p>Added about dialog</p>
      </description>
    </release>
    <release version="1.2.1" date="2019-02-07" urgency="low" type="stable">
      <size type="download">11419440</size>
      <description>
        <p>Removing internal version of SVG renderer to use system default</p>
      </description>
    </release>
    <release version="1.2.0" date="2019-02-07" urgency="low" type="stable">
      <size type="download">12876864</size>
      <description>
        <p>Open to the world!</p>
      </description>
    </release>
    <release version="1.0.2" date="2019-01-25" urgency="low" type="stable">
      <size type="download">237300</size>
      <description>
        <p>Open to the world!</p>
      </description>
    </release>
  </releases>
  <custom>
     <value key="x-appcenter-color-primary">#4e9a06</value>
     <value key="x-appcenter-color-primary-text">rgb(255, 255, 255)</value>
     <value key="x-appcenter-suggested-price">10</value>
  </custom>
</component>






