/* source-view.vala
 *
 * Copyright 2019 Daniel Espinosa Ortiz <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;

public class Svgvi.SourceView : GtkSource.View {
  GtkSource.SearchContext _search;
  GtkSource.SearchSettings _search_settings;
  public GtkSource.SearchContext search { get { return _search; } }
  public GtkSource.SearchSettings search_settings { get { return _search_settings; } }
  construct {
    editable = true;
    can_focus = true;
    can_target = true;
    cursor_visible = true;
    monospace = true;
    auto_indent = true;
    highlight_current_line = true;
    indent_on_tab = true;
    indent_width = 4;
    show_line_marks = true;
    show_line_numbers = true;
    smart_backspace = true;
    var b = get_buffer () as GtkSource.Buffer;
    b.highlight_matching_brackets = true;
    b.highlight_syntax = true;
    var lman = GtkSource.LanguageManager.get_default ();
    ((GtkSource.Buffer) buffer).language = lman.get_language ("xml");
    _search_settings = new GtkSource.SearchSettings ();
    _search = new GtkSource.SearchContext ((GtkSource.Buffer) buffer, _search_settings);
    _search.highlight = true;
  }
  public void forward (string str, Cancellable? cancellable = null) {
    Gtk.TextIter loc;
    TextMark ins = buffer.get_insert ();
    buffer.get_iter_at_mark (out loc, ins);
    _search_settings.search_text = str;
    _search_settings.wrap_around = true;
    _search.forward_async.begin (loc, cancellable, (obj,res)=>{
      TextIter match_start;
      TextIter match_end;
      bool match;
      try {
        if (_search.forward_async.end (res, out match_start, out match_end, out match)) {
          buffer.place_cursor (match_start);
          scroll_mark_onscreen (buffer.get_insert ());
          return;
        }
        _search.backward_async.begin (loc, cancellable, (obj,res)=>{
          try {
            if (_search.backward_async.end (res, out match_start, out match_end, out match)) {
              buffer.place_cursor (match_start);
              scroll_mark_onscreen (buffer.get_insert ());
            }
          } catch (GLib.Error e) {
            warning ("Error: %s", e.message);
          }
        });
      } catch (GLib.Error e) {
        warning ("Error: %s", e.message);
      }
    });
  }
}
