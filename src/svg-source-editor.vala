/* source-view.vala
 *
 * Copyright 2019 Daniel Espinosa Ortiz <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;
[GtkTemplate (ui="/mx/pwmc/Svgvi/svg-source-editor.ui")]
public class Svgvi.SvgSourceEditor : Gtk.Box {
  Svgvi.SourceView _source_view;
  [GtkChild]
  unowned Gtk.SearchEntry esearch;
  [GtkChild]
  unowned Gtk.ScrolledWindow scrolledwindow;
  [GtkChild]
  unowned Gtk.SearchBar searchbar;

  public Svgvi.SourceView source_view { get { return _source_view; } }
  public bool search_active { get { return searchbar.get_search_mode (); } }

  construct {
    _source_view = new Svgvi.SourceView ();
    scrolledwindow.set_child (_source_view);
    _source_view.vexpand = true;
    esearch.changed.connect (()=>{
      _source_view.forward (esearch.text);
    });
  }
  public void enable_search () { searchbar.set_search_mode (true); }
  public void disable_search () { searchbar.set_search_mode (false); }
}
